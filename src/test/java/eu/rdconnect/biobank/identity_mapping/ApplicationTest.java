/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public class ApplicationTest {

    private File mapping;
    private File data;

    public ApplicationTest() {
    }

    @Test(dataProvider = "file-dataprovider")
    public void mainTest(final String msg,
            final String mappingFile, final String dataFile,
            final Boolean mappingFileExists, final Boolean dataFileExists,
            final String[] args) {

        Application.main(args);
        mapping = Paths.get(mappingFile).toFile();
        data = Paths.get(dataFile).toFile();
        assertEquals(mapping.exists(), (boolean) mappingFileExists, "Mapping file: " + msg);
        assertEquals(data.exists(), (boolean) dataFileExists, "Data file: " + msg);
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        if (mapping != null && mapping.exists()) {
            mapping.delete();
        }
        if (data != null && data.exists()) {
            data.delete();
        }
    }

    @DataProvider(name = "file-dataprovider")
    public Object[][] fileDataProvider() throws IOException {
        final String tmpDataOutFile = File.createTempFile("data", ".xlsx").getCanonicalPath();
        final String tmpMappingOutFile = File.createTempFile("mapping", ".xlsx").getCanonicalPath();
        return new Object[][]{
            {
                "Should work with all parameters set to valid values",
                "target/mapping.xlsx", "target/data.xlsx",
                true, true,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should work without an input mapping file",
                "target/mapping.xlsx", "target/data.xlsx",
                true, true,
                new String[]{
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail without an input file",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail without a model file",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail without an output mapping file",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail without an output file",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",}
            },
            {
                "Should fail when input mapping file does not exist",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/doesnotexists.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when input file does not exist",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/doesnotexist.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when model file does not exist",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/doesnotexist.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when the output mapping file already exists",
                tmpMappingOutFile, "target/data.xlsx",
                true, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", tmpMappingOutFile,
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when the output file already exists",
                "target/mapping.xlsx", tmpDataOutFile,
                false, true,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", tmpDataOutFile
                }
            },
            {
                "Should fail when the input mapping file argument is missing",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when the input file argument is missing",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when the model file argument is missing",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model",
                    "-om", "target/mapping.xlsx",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when the output mapping file argument is missing",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om",
                    "-out", "target/data.xlsx"
                }
            },
            {
                "Should fail when the output file argument is missing",
                "target/mapping.xlsx", "target/data.xlsx",
                false, false,
                new String[]{
                    "-im", "src/test/resources/mapping.xlsx",
                    "-in", "src/test/resources/data.xlsx",
                    "-model", "src/test/resources/model.properties",
                    "-om", "target/mapping.xlsx",
                    "-out"
                }
            }
        };
    }
}
