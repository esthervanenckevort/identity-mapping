/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.model;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public class EntityMappingTest {

    @Test(dataProvider = "entitymapping-provider")
    public void testHashCode(final EntityMapping em1, final EntityMapping em2,
            boolean isEqual) {

        Assert.assertEquals((em1.hashCode() == em2.hashCode()), isEqual);
    }

    @Test(dataProvider = "entitymapping-provider")
    public void testEquals(final EntityMapping em1, final EntityMapping em2,
            boolean isEqual) {

        Assert.assertEquals((em1.equals(em2)), isEqual);
    }

    @Test(dataProvider = "entitymapping-nullvalues-provider")
    public void testEqualsNullValues(final EntityMapping em1,
            final EntityMapping em2, boolean isEqual) {
        Assert.assertEquals((em1.equals(em2)), isEqual);
    }

    /**
     * DataProvider for testing equality and hashCodes.
     *
     * @return An Object[][] array with the sets of two EntityMapping objects
     * and a boolean indicating whether they should be considered equal.
     */
    @DataProvider(name = "entitymapping-provider")
    public Object[][] entityMappingProvider() {
        return new Object[][]{
            {
                new EntityMapping(new Entity("entity1"), new Identifier("id1")),
                new EntityMapping(new Entity("entity1"), new Identifier("id2")),
                false
            }, {
                new EntityMapping(new Entity("entity1"), new Identifier("id1")),
                new EntityMapping(new Entity("entity2"), new Identifier("id1")),
                false
            }, {
                new EntityMapping(new Entity("entity1"), new Identifier("id1")),
                new EntityMapping(new Entity("entity2"), new Identifier("id2")),
                false
            }, {
                new EntityMapping(new Entity("entity1"), new Identifier("id1")),
                new EntityMapping(new Entity("entity1"), new Identifier("id1")),
                true
            }, {
                new EntityMapping(new Entity("string1"), new Identifier("string2")),
                new EntityMapping(new Entity("string2"), new Identifier("string1")),
                false
            },
        };
    }

    /**
     * DataProvider for testing equality with null values in play.
     *
     * @return An Object[][] array with the sets of two EntityMapping objects
     * and a boolean indicating whether they should be considered equal.
     */
    @DataProvider(name = "entitymapping-nullvalues-provider")
    public Object[][] entityMappingNullValuesProvider() {
        return new Object[][]{
            {
                new EntityMapping(new Entity("entity1"), new Identifier("id1")),
                null,
                false
            }
        };
    }
}