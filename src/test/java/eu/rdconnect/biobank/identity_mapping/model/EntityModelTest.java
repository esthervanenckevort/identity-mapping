/*
 * Copyright 2014 David van Enckevort.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.model;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.ArrayUtils;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author David van Enckevort
 */
public class EntityModelTest {

    @Test(dataProvider = "equals-dataprovider")
    public void equalsTest(final EntityModel em1, final EntityModel em2,
            final Boolean isSuccess) {
        if (em1 != null) {
            assertEquals(em1.equals(em2), (boolean) isSuccess);
        }
        if (em2 != null) {
            assertEquals(em2.equals(em1), (boolean) isSuccess);
        }
    }

    @Test(dataProvider = "hashCode-dataprovider")
    public void hashCodeTest(final EntityModel em1, final EntityModel em2,
            final Boolean isSuccess) {
        assertEquals(em1.hashCode() == em2.hashCode(), (boolean) isSuccess);
    }

    @DataProvider(name = "hashCode-dataprovider")
    public Object[][] hashCodeDataProvider() {
        Field field1 = new Field("field1");
        Entity entity1 = new Entity("entity1");
        Field field2 = new Field("field2");
        Entity entity2 = new Entity("entity2");
        Map<Field, Entity> mapping1 = new HashMap<>();
        mapping1.put(field1, entity1);
        Map<Field, Entity> mapping2 = new HashMap<>();
        mapping2.put(field2, entity2);
        return new Object[][]{
            {
                new EntityModel(entity1, field1, mapping1),
                new EntityModel(entity1, field1, mapping1),
                true
            },
            {
                new EntityModel(entity1, field1, mapping1),
                new EntityModel(entity1, field1, mapping2),
                false
            },
            {
                new EntityModel(entity1, field1, mapping1),
                new EntityModel(entity1, field2, mapping1),
                false
            },
            {
                new EntityModel(entity1, field1, mapping1),
                new EntityModel(entity2, field1, mapping2),
                false
            },};
    }

    @DataProvider(name = "equals-dataprovider")
    public Object[][] equalsDataProvider() {
        Field field1 = new Field("field1");
        Entity entity1 = new Entity("entity1");
        Field field2 = new Field("field2");
        Entity entity2 = new Entity("entity2");
        Map<Field, Entity> mapping1 = new HashMap<>();
        Map<Field, Entity> mapping2 = new HashMap<>();
        Object[][] equalsTestcases = new Object[][]{
            {
                new EntityModel(entity1, field1, mapping1),
                null,
                false
            },};
        return ArrayUtils.addAll(equalsTestcases, hashCodeDataProvider());
    }
}
