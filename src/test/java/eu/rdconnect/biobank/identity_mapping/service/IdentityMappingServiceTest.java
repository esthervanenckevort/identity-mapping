/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.service;

import eu.rdconnect.biobank.identity_mapping.util.ExcelUtil;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public class IdentityMappingServiceTest {

    private static final String UUID_PATTERN
            = "[A-Za-z0-9]{8}-"
            + "[A-Za-z0-9]{4}-"
            + "[A-Za-z0-9]{4}-"
            + "[A-Za-z0-9]{4}-"
            + "[A-Za-z0-9]{12}";
    private static final String DATA_MISSING_PARTICIPANT_FILE
            = "minimal-data-missing-participant.xlsx";
    private static final String DATA_MISSING_IDENTIFIER_FILE
            = "minimal-data-missing-identifier.xlsx";
    private static final String DATA_MULTIPLE_REFERENCES_FILE
            = "minimal-data-multiple-references.xlsx";
    private static final String DATA_NUMERIC_IDENTIFIERS_FILE
            = "minimal-data-numeric-identifiers.xlsx";
    private static final String DATA_NUMERIC_REFERENCES_FILE
            = "minimal-data-numeric-references.xlsx";
    private static final String DATA_EMPTY_REFERENCE_FILE
            = "minimal-data-empty-reference.xlsx";
    private static final String DATA_MISSING_ENTITY_FILE
            = "minimal-data-missing-entity.xlsx";
    private static final String DATA_FILE = "minimal-data.xlsx";
    private static final String MODEL_FILE = "minimal-model.properties";
    private static final String MAPPING_FILE = "minimal-mapping.xlsx";

    @Test(dataProvider = "map-identites-dataprovider")
    public void mapIdentitiesTest(final Path inputFile, final Path modelFile,
            final Path mappingFile, final Boolean loadMapping,
            final Validator validator) {

        final IdentityMappingService service = new IdentityMappingService();

        try {
            if (loadMapping) {
                service.loadMappingFile(mappingFile);
            }
            Workbook result = service.mapIdentities(inputFile, modelFile);

            validator.validate(result);
        }
        catch (final IOException | InvalidFormatException | DataFormatException | MissingMappingException ex) {

            validator.validateException(ex);
        }
    }

    @DataProvider(name = "map-identites-dataprovider")
    public Object[][] mapIdentitiesDataProvider() throws URISyntaxException {
        // datafile, modelfile, mappingfile, loadmapping, validator, expectedException
        return new Object[][]{
            {
                getFile(DATA_FILE), getFile(MODEL_FILE), getFile(MAPPING_FILE),
                false, new StructureValidator()
            },
            {
                getFile(DATA_FILE), getFile(MODEL_FILE), getFile(MAPPING_FILE),
                true, new OutputValidator()
            },
            {
                getFile(DATA_MISSING_IDENTIFIER_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new ExceptionValidator(DataFormatException.class)
            },
            {
                getFile(DATA_NUMERIC_REFERENCES_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new ExceptionValidator(DataFormatException.class)
            },
            {
                getFile(DATA_NUMERIC_IDENTIFIERS_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new ExceptionValidator(DataFormatException.class)
            },
            {
                getFile(DATA_MISSING_PARTICIPANT_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new ExceptionValidator(MissingMappingException.class)
            },
            {
                getFile(DATA_MISSING_PARTICIPANT_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), true, new ResolveFromMappingFileValidator()
            },
            {
                getFile(DATA_MULTIPLE_REFERENCES_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new ReferencesValidator()
            },
            {
                getFile(DATA_EMPTY_REFERENCE_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new EmptyReferencesValidator()
            },
            {
                getFile(DATA_MISSING_ENTITY_FILE), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new ExceptionValidator(DataFormatException.class)
            },
            {
                getFile("minimal-data-missing-id.xlsx"), getFile(MODEL_FILE),
                getFile(MAPPING_FILE), false, new ExceptionValidator(DataFormatException.class),
            }
        };
    }

    private static class ResolveFromMappingFileValidator implements Validator {

        @Override
        public void validate(Workbook result) {
            final Sheet sheet = result.getSheet("Sample");
            assertFalse(ExcelUtil.isBlankCellOrNull(sheet.getRow(3).getCell(1)));
            assertFalse(ExcelUtil.isBlankCellOrNull(sheet.getRow(6).getCell(1)));
            assertFalse(ExcelUtil.isBlankCellOrNull(sheet.getRow(9).getCell(1)));
        }

        @Override
        public void validateException(Exception ex) {
            fail();
        }
    }

    private static class EmptyReferencesValidator implements Validator {

        @Override
        public void validate(final Workbook result) {
            final Sheet sheet = result.getSheet("Sample");
            assertTrue(ExcelUtil.isBlankCellOrNull(sheet.getRow(7).getCell(1)));
            assertTrue(ExcelUtil.isBlankCellOrNull(sheet.getRow(8).getCell(1)));
            assertTrue(ExcelUtil.isBlankCellOrNull(sheet.getRow(9).getCell(1)));
        }

        @Override
        public void validateException(final Exception ex) {
            fail();
        }
    }

    private static class ReferencesValidator implements Validator {

        @Override
        public void validate(final Workbook result) {
            final Sheet sheet = result.getSheet("Sample");

            validateMultipleReferences(sheet, 1, 1);
            validateMultipleReferences(sheet, 2, 1);
            validateMultipleReferences(sheet, 3, 1);
        }

        private void validateMultipleReferences(final Sheet sheet, final int rowNum,
                final int cellNum) {
            final Cell cell = sheet.getRow(rowNum).getCell(cellNum);
            assertFalse(ExcelUtil.isBlankCellOrNull(cell));
            final String data = cell.getStringCellValue();
            final String[] identifiers = data.split(",");
            assertEquals(identifiers.length, 2);
            assertTrue(identifiers[0].trim().matches(UUID_PATTERN));
            assertTrue(identifiers[1].trim().matches(UUID_PATTERN));
        }

        @Override
        public void validateException(Exception ex) {
            fail();
        }
    }

    private static class StructureValidator implements Validator {

        @Override
        public void validate(final Workbook result) {
            assertNotNull(result, "mapIdentities should return a workbook.");
            assertEquals(result.getNumberOfSheets(), 2, "Expected two sheets in the result.");
            final Sheet participant = result.getSheet("Participant");
            assertNotNull(participant, "There should be a sheet called 'Participant'");
            for (Row row : ExcelUtil.getRows(participant)) {
                checkId(row);
            }
            final Sheet sample = result.getSheet("Sample");
            assertNotNull(sample, "There should be a sheet called 'Sample'");
            for (Row row : ExcelUtil.getRows(sample)) {
                checkId(row);
                if (row.getRowNum() > 0) {
                    String participantRef = row.getCell(1).getStringCellValue();
                    assertTrue(participantRef.matches(UUID_PATTERN), "The participant cell should contain a UUID");
                }
            }
        }

        private void checkId(Row row) {
            if (row.getRowNum() > 0) {
                final String id = row.getCell(0).getStringCellValue();
                assertTrue(id.matches(UUID_PATTERN), "The id cell should contain a UUID");
            }
        }

        @Override
        public void validateException(final Exception ex) {
            fail();
        }

    }

    private static class OutputValidator extends StructureValidator {

        @Override
        public void validate(final Workbook result) {
            super.validate(result);
            for (final Row row : ExcelUtil.getRows(result.getSheet("Participant"))) {
                for (final Cell cell : ExcelUtil.getCells(row)) {
                    assertEquals(cell.getStringCellValue(),
                            participants[cell.getRowIndex()][cell.getColumnIndex()]);
                }
            }
        }

        private final String[][] participants = {
            {"id", "name", "age"},
            {"eda8dfbe-b270-4e9f-aca4-ebbb7bcf8350", "Bert", "4"},
            {"0a3c1d87-0c53-4724-be3c-0daeaff4c47e", "Ernie", "5"},
            {"0b9c31ce-9c6c-40e6-a2dd-ffe20f8ee20b", "Pino", "7"},};
    }

    private Path getFile(final String file) throws URISyntaxException {
        return Paths.get(getClass().getResource(file).toURI());
    }
}
