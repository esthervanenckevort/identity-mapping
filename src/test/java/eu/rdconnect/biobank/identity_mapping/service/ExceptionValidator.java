/*
 * Copyright 2014 David van Enckevort.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.service;

import org.apache.poi.ss.usermodel.Workbook;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

/**
 * Validator that verifies that the test throws an exception.
 *
 * @author David van Enckevort
 */
class ExceptionValidator implements Validator {

    /**
     * The message of describing the test.
     */
    private final String message;
    /**
     * The expected exception.
     */
    private final Class<? extends Throwable> exceptionClass;

    /**
     * Constructor.
     *
     * @param exceptionClass the expected exception.
     */
    ExceptionValidator(Class<? extends Throwable> exceptionClass) {
        this.exceptionClass = exceptionClass;
        message = String.format("Expected an exception of class %s.",
                exceptionClass.getCanonicalName());
    }


    /**
     * Constructor.
     *
     * @param messageText the text describing the test.
     * @param exception the exception that we expect.
     */
    ExceptionValidator(final String messageText,
            final Class<? extends Throwable> exception) {

        message = messageText;
        this.exceptionClass = exception;
    }


    @Override
    public void validate(final Workbook result) {
        fail(message);
    }

    @Override
    public void validateException(final Exception ex) {
        assertEquals(ex.getClass(), exceptionClass, message);
    }
}
