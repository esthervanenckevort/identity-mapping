/*
 * Copyright 2014 David van Enckevort.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author David van Enckevort
 */
public class LoadModelTest {

    @Test(dataProvider = "model-dataprovider")
    public void loadModelTest(final String input, final String model,
            final Validator validator) throws URISyntaxException {

        final IdentityMappingService service = new IdentityMappingService();
        final Path inputFile = Paths.get(getClass().getResource(input).toURI());
        final Path modelFile = Paths.get(getClass().getResource(model).toURI());

        try {

            final Workbook result = service.mapIdentities(inputFile, modelFile);
            validator.validate(result);

        }
        catch (final IOException | InvalidFormatException |
                DataFormatException | MissingMappingException ex) {

            validator.validateException(ex);
        }
    }

    @DataProvider(name = "model-dataprovider")
    public Object[][] modelDataProvider() {
        return new Object[][]{
            {"minimal-data.xlsx", "minimal-model.properties", new NotNullValidator()},
            {"minimal-data.xlsx", "minimal-model-missing-attribute-name.properties",
                new ExceptionValidator("Missing attribute name should raise an exception.", DataFormatException.class)},
            {"minimal-data.xlsx", "minimal-model-missing-entities.properties",
                new ExceptionValidator("Missing entities property should raise an exception.", DataFormatException.class)},
            {"minimal-data.xlsx", "minimal-model-missing-entity-reference.properties",
                new ExceptionValidator("Reference missing entity name should raise an exception.", DataFormatException.class)},
            {"minimal-data.xlsx", "minimal-model-missing-entity.properties",
                new ExceptionValidator("Missing entity should raise an exception.", DataFormatException.class)},
        };
    }
}
