/*
 * Copyright 2014 David van Enckevort.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.service;

import org.apache.poi.ss.usermodel.Workbook;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

/**
 *
 * @author David van Enckevort
 */
class NotNullValidator implements Validator {
    private static final String message = "method should return a valid workbook.";
    @Override
    public void validate(final Workbook result) {
        assertNotNull(result, message);
    }

    @Override
    public void validateException(final Exception ex) {
        fail(message);
    }
}
