/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.service;

import org.testng.annotations.DataProvider;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public class TestDataProvider {

    @DataProvider(name = "minimal-mapping")
    public static Object[][] getMinimalMapping() {
        return new Object[][] {
            {"Sample", "s_20001", "6ADC6ACD-45D3-4839-BA34-6DB8E24A609E"},
            {"Sample", "s_20002", "79A5190E-25D4-4AC2-A6CB-1879B5546391"},
            {"Sample", "s_20003", "16ECCA97-1400-4DE6-8975-846281995CCD"},
            {"Sample", "s_20004", "468AE970-65C2-42E0-B30C-53DE5DB3086E"},
            {"Sample", "s_20005", "782AA0AA-7AC5-4983-8E9E-E69FB6EA80A2"},
            {"Sample", "s_20006", "E929B273-174C-41B9-9386-0864ABAC06E6"},
            {"Sample", "s_20007", "9730C2E6-1FFD-4AEC-9921-A231F0FFBC8D"},
            {"Sample", "s_20008", "ACB53E9F-1F60-4824-905B-7DEED84B7048"},
            {"Sample", "s_20009", "AAF4A53F-FB8D-4179-B8A1-8559FD841F8F"},
            {"Participant", "p_10001", "EDA8DFBE-B270-4E9F-ACA4-EBBB7BCF8350"},
            {"Participant", "p_10002", "0A3C1D87-0C53-4724-BE3C-0DAEAFF4C47E"},
            {"Participant", "p_10003", "0B9C31CE-9C6C-40E6-A2DD-FFE20F8EE20B"},};
    }

}
