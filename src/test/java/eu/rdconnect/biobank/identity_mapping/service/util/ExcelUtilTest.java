/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.service.util;

import eu.rdconnect.biobank.identity_mapping.util.ExcelUtil;
import java.util.Date;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public class ExcelUtilTest {
    private static final String CELLTYPE_DATAPROVIDER = "celltype-dataprovider";
    private static final String CELL_DATAPROVIDER = "cell-dataprovider";

    public ExcelUtilTest() {
    }

    @Test(dataProvider = CELL_DATAPROVIDER)
    public void getCellCoordinatesTest(final Cell cell, final String result) {
        assertEquals(ExcelUtil.getCellCoordinates(cell), result, "Excel Cell Coordinates should match");
    }

    @Test(dataProvider = CELLTYPE_DATAPROVIDER)
    public void isTextCellTest(final Cell cell, final Boolean isBlank,
            final Boolean isText, final String msg) {
        assertEquals(ExcelUtil.isTextCell(cell), (boolean)isText, msg);
    }

    @Test(dataProvider = CELLTYPE_DATAPROVIDER)
    public void isBlankCellOrNullTest(final Cell cell, final Boolean isBlank,
            final Boolean isText, final String msg) {
        assertEquals(ExcelUtil.isBlankCellOrNull(cell), (boolean)isBlank, msg);
    }

    @DataProvider(name = CELLTYPE_DATAPROVIDER)
    public Object[][] cellTypeDataProvider() {
        final Workbook wb = new HSSFWorkbook();
        final Sheet sheet = wb.createSheet("test");
        final Row row = sheet.createRow(0);
        int cell = 1;
        final Cell blankCell = row.createCell(cell++, Cell.CELL_TYPE_BLANK);
        final Cell booleanTrueCell = row.createCell(cell++, Cell.CELL_TYPE_BOOLEAN);
        booleanTrueCell.setCellValue(true);
        final Cell booleanFalseCell = row.createCell(cell++, Cell.CELL_TYPE_BOOLEAN);
        booleanFalseCell.setCellValue(false);
        final Cell blankStringCell = row.createCell(cell++, Cell.CELL_TYPE_STRING);
        blankStringCell.setCellValue("");
        final Cell unsetStringCell = row.createCell(cell++, Cell.CELL_TYPE_STRING);
        final Cell stringCell = row.createCell(cell++, Cell.CELL_TYPE_STRING);
        stringCell.setCellValue("test");
        final Cell numericCell = row.createCell(cell++, Cell.CELL_TYPE_NUMERIC);
        numericCell.setCellValue(1.0);
        final Cell numericDateCell = row.createCell(cell++, Cell.CELL_TYPE_NUMERIC);
        numericDateCell.setCellValue(new Date());
        return new Object[][] {
            { blankCell, true, false, "blank cell" },
            { booleanTrueCell, false, false, "boolean true" },
            { booleanFalseCell, false, false, "boolean false" },
            { blankStringCell, true, true, "blank string" },
//            Skip test since Cell#toString() throws a null-pointer exception.
//            { unsetStringCell, true, true, "unset string" },
            { stringCell, false, true, "string test" },
            { numericCell, false, false, "numeric 1.0" },
            { numericDateCell, false, false, "numeric date" },
            { null, true, false, "null"}
        };
    }

    @DataProvider(name = CELL_DATAPROVIDER)
    public Object[][] cellDataProvider() {
        final Workbook wb = new HSSFWorkbook();
        final Sheet sheet = wb.createSheet("test");
        final Row row1 = sheet.createRow(0);
        final Cell A1 = row1.createCell(0);
        final Cell AA1 = row1.createCell(26);
        final Row row100 = sheet.createRow(99);
        final Cell A100 = row100.createCell(0);
        final Cell AA100 = row100.createCell(26);
        return new Object[][]{
            {A1, "test!A1"},
            {AA1, "test!AA1"},
            {A100, "test!A100"},
            {AA100, "test!AA100"}
        };
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
