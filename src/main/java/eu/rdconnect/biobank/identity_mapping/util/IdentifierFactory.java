/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.util;

import eu.rdconnect.biobank.identity_mapping.model.Identifier;

/**
 * Factory class to create Identifier instances out of a String value.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class IdentifierFactory implements StringValueFactory<Identifier> {

    @Override
    public Identifier create(final String value) {
        return new Identifier(value);
    }

    /**
     * Get an iterable that iterates over all the comma separated values of
     * the given String.
     *
     * @param value the comma separated list of values.
     * @return An Iterable that iterates over the values. Each value is returned
     * as an instance of {@link Identifier} holding the value.
     */
    public static Iterable<Identifier> getIterable(final String value) {
        return new StringIterableImpl<>(value, new IdentifierFactory());
    }

}
