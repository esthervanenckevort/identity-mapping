/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.util;

/**
 * Interface for a Factory class that can create entities that wrap a String
 * value. This is a work-around for the fact that Java doesn't have
 * value-classes as a more lightweight type-safe variables for String values.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 * @param <T> The actual class that wraps a string value.
 */
interface StringValueFactory<T> {
    /**
     * Factory method to create a new instance of a class that wraps a String.
     *
     * @param value the value of the wrapped string.
     * @return an instance of T holding the given String value.
     */
    T create(String value);
}
