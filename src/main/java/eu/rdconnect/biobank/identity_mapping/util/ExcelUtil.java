/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.util;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;

/**
 * Utility class that encapsulates a few of the rough edges of Apache POI in
 * working with Excel workbooks.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class ExcelUtil {

    /**
     * Constructor. This constructor is private to prevent incidential
     * instantiation of this utility class.
     */
    private ExcelUtil() {
    }

    /**
     * Get an Iterable over the cells in the given row.
     *
     * @param row the row over which cells we want to iterate.
     * @return An iterable over the cells in the row.
     */
    public static Iterable<Cell> getCells(final Row row) {
        return new Iterable<Cell>() {

            @Override
            public Iterator<Cell> iterator() {
                return row.cellIterator();
            }
        };
    }

    /**
     * Get an Iterable over the rows in the given sheet.
     *
     * @param sheet the sheet over which rows we want to iterate.
     * @return An iterable over the rows in the sheet.
     */
    public static Iterable<Row> getRows(final Sheet sheet) {
        return new Iterable<Row>() {

            @Override
            public Iterator<Row> iterator() {
                return sheet.rowIterator();
            }
        };
    }

    /**
     * Get an Iterable over the sheets in the given workbook.
     *
     * @param workbook the workbook over which sheets we want to iterate.
     * @return An iterable over the sheets in the workbook.
     */
    public static Iterable<Sheet> getSheets(final Workbook workbook) {
        return new Iterable<Sheet>() {

            @Override
            public Iterator<Sheet> iterator() {
                return new Iterator<Sheet>() {
                    private int index = 0;

                    @Override
                    public boolean hasNext() {
                        return index < workbook.getNumberOfSheets();
                    }

                    @Override
                    public Sheet next() {
                        if (!hasNext()) {
                            throw new NoSuchElementException();
                        }
                        final Sheet sheet = workbook.getSheetAt(index);
                        index++;
                        return sheet;
                    }
                };
            }
        };
    }

    /**
     * Get the cell Coordinates for a cell.
     *
     * @param cell the cell for which to get the coordinates.
     * @return A string expressing the coordinates of the cell in the following
     * format sheetname followed by a exclamation mark, followed by the
     * alphabetic column number and the row number, e.g.: Sheet1!A200.
     */
    public static String getCellCoordinates(final Cell cell) {
        final int columnIndex = cell.getColumnIndex();
        final String sheetName = cell.getSheet().getSheetName();
        final String column = CellReference.convertNumToColString(columnIndex);
        return String.format("%s!%s%d",
                sheetName, column, cell.getRowIndex() + 1);
    }

    /**
     * Helper method to get a sheet by the given name in the workbook. This
     * method will create the sheet if necessary.
     *
     * @param workbook the workbook in which to create the sheet.
     * @param name the name of the sheet to create.
     * @return the Sheet instance.
     */
    public static Sheet getOrCreateSheet(final Workbook workbook,
            final String name) {

        if (workbook.getSheet(name) != null) {
            return workbook.getSheet(name);
        } else {
            return workbook.createSheet(name);
        }
    }

    /**
     * Helper method to test if the cell is a Text cell. This method is null
     * safe.
     *
     * @param cell the cell for which we want to check if it is a text cell, may
     * be null.
     * @return true if the cell is a text cell, false otherwise.
     */
    public static boolean isTextCell(final Cell cell) {
        return cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING;
    }

    /**
     * Helper method to test if the text cell is blank (i.e. null, empty or only
     * contains whitespace characters).
     *
     * @param cell the cell.
     * @return true if the cell is considered blank.
     */
    private static boolean isBlankTextCell(final Cell cell) {
        return cell.getCellType() == Cell.CELL_TYPE_STRING
                && StringUtils.isBlank(cell.getStringCellValue());
    }

    /**
     * Helper method to test if the cell is a Blank cell or null.
     *
     * @param cell the cell to check.
     * @return true if the cell is blank or null, false otherwise.
     */
    public static boolean isBlankCellOrNull(final Cell cell) {
        return cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK
                || isBlankTextCell(cell);
    }
}
