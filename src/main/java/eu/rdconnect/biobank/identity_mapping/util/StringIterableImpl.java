/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Generic implementation of an Iterable and Iterator class to iterate over a
 * comma separated string of string representations of entity values.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 * @param <T> the entity class of the string entity values.
 */
final class StringIterableImpl<T> implements Iterable<T>, Iterator<T> {

    /**
     * Current position in the parts array.
     */
    private int pos = 0;
    /**
     * Array holding the individual identifiers.
     */
    private final String[] parts;
    /**
     * Factory class to create a new instance of T.
     */
    private final StringValueFactory<T> factory;

    /**
     * Constructor.
     *
     * @param identifiers the string holding the comma separated values.
     * @param factoryImpl the implementation of the factory class.
     */
    public StringIterableImpl(final String identifiers,
            final StringValueFactory<T> factoryImpl) {
        parts = identifiers.split(",");
        factory = factoryImpl;
    }

    @Override
    public Iterator<T> iterator() {
        pos = 0;
        return this;
    }

    @Override
    public boolean hasNext() {
        return pos < parts.length;
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        final String id = parts[pos].trim();
        pos++;
        return factory.create(id);
    }

}
