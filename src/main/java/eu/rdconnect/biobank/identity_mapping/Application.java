/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping;

import eu.rdconnect.biobank.identity_mapping.service.DataFormatException;
import eu.rdconnect.biobank.identity_mapping.service.IdentityMappingService;
import eu.rdconnect.biobank.identity_mapping.service.MappingServiceException;
import eu.rdconnect.biobank.identity_mapping.service.MissingMappingException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Application to run the identity mapping service.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class Application {

    /**
     * Logger instance for this class.
     */
    private static final Logger LOG;
    /**
     * Commandline option for the output file.
     */
    private static final String ARG_OUTPUT_FILE = "out";
    /**
     * Commandline option for the input file.
     */
    private static final String ARG_INPUT_FILE = "in";
    /**
     * Commandline option for the output mapping file.
     */
    private static final String ARG_OUTPUT_MAPPING_FILE = "om";
    /**
     * Commandline option for the input mapping file.
     */
    private static final String ARG_INPUT_MAPPING_FILE = "im";
    /**
     * Commandline option for the model definition file.
     */
    private static final String ARG_MODEL_FILE = "model";
    /**
     * Parser to parse the commandline options.
     */
    private static final PosixParser PARSER = new PosixParser();
    /**
     * Commandline options for this application.
     */
    private static final Options OPTIONS = new Options();
    /**
     * Location of the input file.
     */
    private Path inputFile;
    /**
     * Location of the output file.
     */
    private Path outputFile;
    /**
     * Location of the input mapping file.
     */
    private Path inputMappingFile;
    /**
     * Location of the output mapping file.
     */
    private Path outputMappingFile;
    /**
     * Location of the model definition file.
     */
    private Path modelFile;

    static {
        LOG = Logger.getLogger(Application.class.getName());
        setCommandLineOptions();
    }

    /**
     * Main entry point for the application.
     *
     * @param args the commandline arguments.
     */
    public static void main(final String[] args) {
        try {
            Application app = new Application(args);
            app.run();
        } catch (final ParseException ex) {
            LOG.log(Level.INFO, ex.getMessage());
            Application.help();
        } catch (final IOException | InvalidFormatException
                | MappingServiceException ex) {
            LOG.log(Level.SEVERE, ex.getMessage());
        }
    }

    /**
     * Parse the commandline options and set the fields.
     *
     * @param args the commandline options for the application.
     * @throws ParseException when an invalid commandline option is given.
     */
    private void parseCommandLine(final String[] args) throws ParseException {
        final CommandLine cmd = PARSER.parse(OPTIONS, args);
        final String inputMapping;
        inputMapping = cmd.getOptionValue(ARG_INPUT_MAPPING_FILE);
        final String outputMapping;
        outputMapping = cmd.getOptionValue(ARG_OUTPUT_MAPPING_FILE);
        final String input = cmd.getOptionValue(ARG_INPUT_FILE);
        final String output = cmd.getOptionValue(ARG_OUTPUT_FILE);
        final String model = cmd.getOptionValue(ARG_MODEL_FILE);

        inputFile = Paths.get(input);
        outputFile = Paths.get(output);
        if (inputMapping != null) {
            inputMappingFile = Paths.get(inputMapping);
        }
        outputMappingFile = Paths.get(outputMapping);
        modelFile = Paths.get(model);

        // Validate files
        if (inputMappingFile != null && !inputMappingFile.toFile().exists()) {
            throw new ParseException("Input mappping file should exists.");
        }
        if (outputFile.toFile().exists()
                || outputMappingFile.toFile().exists()) {
            throw new ParseException("Output files should not exists.");
        }
        if (!inputFile.toFile().exists()) {
            throw new ParseException("Input file should exists.");
        }
        if (!modelFile.toFile().exists()) {
            throw new ParseException("Model file should exists.");
        }
    }

    /**
     * Constructor.
     *
     * @param args the commandline arguments for the application.
     * @throws ParseException when an invalid commandline option is given.
     */
    private Application(final String[] args) throws ParseException {
        parseCommandLine(args);
    }

    /**
     * Create the commandline options for the application.
     */
    private static void setCommandLineOptions() {
        Option option;

        option = new Option(ARG_INPUT_MAPPING_FILE, true,
                "Input mapping file");
        option.setRequired(false);
        OPTIONS.addOption(option);

        option = new Option(ARG_OUTPUT_MAPPING_FILE, true,
                "Output mapping file");
        option.setRequired(true);
        OPTIONS.addOption(option);

        option = new Option(ARG_INPUT_FILE, true, "Input data file");
        option.setRequired(true);
        OPTIONS.addOption(option);

        option = new Option(ARG_OUTPUT_FILE, true, "Output data file");
        option.setRequired(true);
        OPTIONS.addOption(option);

        option = new Option(ARG_MODEL_FILE, true, "Model definition file");
        option.setRequired(true);
        OPTIONS.addOption(option);
    }

    /**
     * Run the mapping process.
     *
     * @throws IOException If there is an error writing one of the
     * files.
     * @throws InvalidFormatException If one of the input excel files is
     * malformed.
     * @throws DataFormatException when the data in one of the Excel workbooks
     * does not conform to the model.
     * @throws MissingMappingException when no mapping is found for a referenced
     * identifier.
     */
    private void run() throws IOException, InvalidFormatException,
            DataFormatException, MissingMappingException {

        final IdentityMappingService idMappingService;
        idMappingService = new IdentityMappingService();

        if (inputMappingFile != null) {
            idMappingService.loadMappingFile(inputMappingFile);
        }

        final Workbook output;
        output = idMappingService.mapIdentities(inputFile, modelFile);
        output.write(Files.newOutputStream(outputFile));

        final Workbook outputMapping = idMappingService.getMapping();
        outputMapping.write(Files.newOutputStream(outputMappingFile));
    }

    /**
     * Print the help message for the commandline options.
     */
    private static void help() {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Identity mapping service", OPTIONS);
    }
}
