/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.service;

import eu.rdconnect.biobank.identity_mapping.model.Entity;
import eu.rdconnect.biobank.identity_mapping.model.EntityMapping;
import eu.rdconnect.biobank.identity_mapping.model.EntityModel;
import eu.rdconnect.biobank.identity_mapping.model.Field;
import eu.rdconnect.biobank.identity_mapping.model.Identifier;
import eu.rdconnect.biobank.identity_mapping.util.EntityFactory;
import eu.rdconnect.biobank.identity_mapping.util.ExcelUtil;
import eu.rdconnect.biobank.identity_mapping.util.IdentifierFactory;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Service to map local identifiers to opaque UUIDs.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class IdentityMappingService {

    /**
     * Logger instance for this class.
     */
    private static final Logger LOG;
    /**
     * Exception message when the ID column is missing in the sheet for an
     * entity.
     */
    private static final String EX_IDCOLUM_MISSING
            = "The entity is missing an identity column: ";
    /**
     * Exception message when the reference column is missing in the sheet for
     * an entity.
     */
    private static final String EX_REFCOLUM_MISSING
            = "The entity is missing a reference column: ";
    /**
     * Exception message when an entity is missing from the workbook.
     */
    private static final String EX_ENTITY_MISSING = "The entity is missing: ";

    static {
        LOG = Logger.getLogger(IdentityMappingService.class.getName());
    }

    /**
     * Map holding a mapping of local identities to UUIDs for lookup of the
     * replacement ID when replacing references.
     */
    private final Map<EntityMapping, UUID> identityMap;

    /**
     * Constructor.
     */
    public IdentityMappingService() {
        identityMap = new HashMap<>();
    }

    /**
     * Loader for a mapping file holding pre-existing mappings of local
     * identifiers to UUIDs.
     *
     * <em>NOTE:</em> It is possible to load multiple mapping files, which will
     * be merged in memory.
     *
     * @param mappingFile the file to load. This should be a valid Excel
     * workbook file, either in XLS or XLSX format.
     *
     * @throws IOException when an I/O error occurs loading the file.
     * @throws InvalidFormatException when the Excel workbook is malformed.
     * @throws DataFormatException when the Excel workbook does not match the
     * data model.
     */
    public void loadMappingFile(final Path mappingFile)
            throws IOException, InvalidFormatException, DataFormatException {

        final Workbook workbook;
        final InputStream stream = Files.newInputStream(mappingFile);
        workbook = WorkbookFactory.create(stream);

        for (final Sheet sheet : ExcelUtil.getSheets(workbook)) {
            loadMappingSheet(sheet);
        }

    }

    /**
     * Load a sheet with a mappings.
     *
     * @param sheet the sheet holding the mapping.
     * @throws DataFormatException when the sheet does not match the model.
     */
    private void loadMappingSheet(final Sheet sheet)
            throws DataFormatException {

        final Entity entity = new Entity(sheet.getSheetName());

        for (final Row row : ExcelUtil.getRows(sheet)) {
            final Cell localIDCell = row.getCell(0);
            final Cell globalIDCell = row.getCell(1);

            if (!ExcelUtil.isTextCell(localIDCell)
                    || !ExcelUtil.isTextCell(globalIDCell)) {
                throw new DataFormatException("Mapping is incomplete on row: "
                        + row.getRowNum() + 1);
            }
            final UUID globalID;
            globalID = UUID.fromString(globalIDCell.getStringCellValue());
            final Identifier localID;
            localID = new Identifier(localIDCell.getStringCellValue());
            final EntityMapping mapping;
            mapping = new EntityMapping(entity, localID);
            identityMap.put(mapping, globalID);
        }
    }

    /**
     * Get a Excel workbook holding the mappings of local identifiers to UUIDs.
     *
     * This will return a workbook ordered in separate sheets for each entity.
     * Each sheet has the local identifier in column A and the global UUID in
     * column B.
     *
     * @return An Excel workbook object with the mappings.
     */
    public Workbook getMapping() {
        final Workbook workbook = new XSSFWorkbook();
        for (final EntityMapping key : identityMap.keySet()) {

            final String sheetName = key.getEntity().getName();
            final Sheet sheet = ExcelUtil.getOrCreateSheet(workbook, sheetName);
            final Row row = sheet.createRow(sheet.getLastRowNum() + 1);
            final String localID = key.getIdentifier().getValue();
            final String globalID = identityMap.get(key).toString();

            row.createCell(0).setCellValue(localID);
            row.createCell(1).setCellValue(globalID);
        }
        return workbook;
    }

    /**
     * Replace the identity fields in the workbook with an opaque global ID.
     *
     * @param inputFile the file to update.
     * @param modelFile the file describing the entity model used.
     * @return A workbook with the local IDs replaced.
     * @throws IOException when an I/O error occurs while reading the files.
     * @throws InvalidFormatException when the input workbook is malformed.
     * @throws DataFormatException when one of the Excel workbooks does not
     * match the model.
     * @throws MissingMappingException when no mapping for a referenced
     * identifier exists
     */
    public Workbook mapIdentities(final Path inputFile, final Path modelFile)
            throws IOException, InvalidFormatException, DataFormatException,
            MissingMappingException {

        final List<EntityModel> models;
        models = getEntityModels(modelFile);
        final Workbook workbook;
        workbook = WorkbookFactory.create(inputFile.toFile());

        replaceIDs(workbook, models);
        replaceReferences(workbook, models);

        return workbook;
    }

    /**
     * Replace the local IDs in the workbook with opaque global IDs.
     *
     * @param workbook the workbook to update.
     * @param models the models describing the entities to update.
     * @throws DataFormatException when an entity defined in the model is not
     * present in the input file.
     */
    private void replaceIDs(final Workbook workbook,
            final List<EntityModel> models) throws DataFormatException {

        for (final EntityModel model : models) {

            final Sheet sheet = workbook.getSheet(model.getEntity().getName());

            if (sheet == null) {
                throw new DataFormatException(EX_ENTITY_MISSING
                        + model.getEntity().getName());
            }
            final int idColumn = getIdentityColumn(sheet, model.getField());
            if (idColumn == -1) {
                throw new DataFormatException(EX_IDCOLUM_MISSING
                        + model.getEntity().getName());
            }
            for (final Row row : ExcelUtil.getRows(sheet)) {

                final Cell cell;
                cell = row.getCell(idColumn, Row.CREATE_NULL_AS_BLANK);

                if (!isFieldHeader(model.getField(), cell)) {
                    replaceIdentity(cell, model);
                }
            }
        }
    }

    /**
     * Replace the local ID with an opaque global identity.
     *
     * @param cell the cell to update.
     * @param model the entity model describing the entity.
     * @throws DataFormatException when the identifier is empty or not a text
     * string.
     */
    private void replaceIdentity(final Cell cell, final EntityModel model)
            throws DataFormatException {

        if (ExcelUtil.isBlankCellOrNull(cell) || !ExcelUtil.isTextCell(cell)) {
            final String msg = String.format(
                    "Unreadable identity column: cell %s must be a string type",
                    ExcelUtil.getCellCoordinates(cell));
            LOG.log(Level.SEVERE, msg);
            throw new DataFormatException(msg);
        }
        final Identifier localID;
        localID = new Identifier(cell.getStringCellValue());
        final Entity entity = model.getEntity();
        final EntityMapping mapping = new EntityMapping(entity, localID);
        if (!identityMap.containsKey(mapping)) {
            identityMap.put(mapping, UUID.randomUUID());
        }
        cell.setCellValue(identityMap.get(mapping).toString());
    }

    /**
     * Check if the cell is the header for a field. This method compares the
     * cell value with the field name without consideration of the case.
     *
     * @param field the field to check for.
     * @param cell the cell to check.
     * @return true if the cell holds the header.
     */
    private boolean isFieldHeader(final Field field, final Cell cell) {
        return !ExcelUtil.isBlankCellOrNull(cell)
                && ExcelUtil.isTextCell(cell)
                && field.getName().equalsIgnoreCase(cell.getStringCellValue());
    }

    /**
     * Replace the local ID references to instances defined in the entity models
     * with a global ID.
     *
     * @param workbook the workbook to update.
     * @param models the entity models describing the referenced entity classes.
     * @throws MissingMappingException when no mapping for an identifier can be
     * found.
     * @throws DataFormatException when an identifier reference cell is not a
     * text cell.
     */
    private void replaceReferences(final Workbook workbook,
            final List<EntityModel> models)
            throws DataFormatException, MissingMappingException {

        for (final EntityModel model : models) {
            final Sheet sheet = workbook.getSheet(model.getEntity().getName());
            if (sheet == null) {
                throw new DataFormatException(EX_ENTITY_MISSING
                        + model.getEntity());
            }
            updateReferencesForSheet(model, sheet);
        }
    }

    /**
     * Replace the local ID references to instances defined in the entity model
     * with a global ID.
     *
     * @param model the entity model describing the referenced entity class.
     * @param sheet the sheet to update.
     * @throws MissingMappingException when no mapping for an identifier can be
     * found.
     * @throws DataFormatException when an identifier reference cell is not a
     * text cell.
     */
    private void updateReferencesForSheet(
            final EntityModel model, final Sheet sheet)
            throws MissingMappingException, DataFormatException {

        for (final Field field : model.getEntityReferences().keySet()) {

            final Entity referencedEntity;
            referencedEntity = model.getEntityReferences().get(field);
            final int idColumn = getIdentityColumn(sheet, field);

            if (idColumn == -1) {
                throw new DataFormatException(EX_REFCOLUM_MISSING
                        + field.getName());
            }
            for (final Row row : ExcelUtil.getRows(sheet)) {

                final Cell cell = row.getCell(idColumn);

                if (!isFieldHeader(field, cell)
                        && !ExcelUtil.isBlankCellOrNull(cell)) {

                    updateReferencesForCell(cell, referencedEntity);
                }
            }
        }
    }

    /**
     * Replace the local ID references to instances of the given entity class
     * with a global ID.
     *
     * @param cell the cell to process
     * @param entity the entity class to which the IDs in the cell refer.
     * @throws MissingMappingException when no mapping can be found.
     * @throws DataFormatException when an identifier is not a text cell.
     */
    private void updateReferencesForCell(final Cell cell, final Entity entity)
            throws MissingMappingException, DataFormatException {

        if (!ExcelUtil.isTextCell(cell)) {
            final String msg = String.format(
                    "Unreadable identity column: cell %s must be a string type",
                    ExcelUtil.getCellCoordinates(cell));
            LOG.log(Level.SEVERE, msg);
            throw new DataFormatException(msg);
        }

        if (ExcelUtil.isBlankCellOrNull(cell)) {
            LOG.log(Level.INFO, "Empty reference cell %s",
                    ExcelUtil.getCellCoordinates(cell));
        } else {
            final String ids = cell.getStringCellValue();
            final StringBuilder builder = new StringBuilder();

            for (final Identifier id : IdentifierFactory.getIterable(ids)) {

                final EntityMapping mapping;
                mapping = new EntityMapping(entity, id);

                if (!identityMap.containsKey(mapping)) {
                    final String msg = String.format(
                            "No UUID found for value in cell %s.",
                            ExcelUtil.getCellCoordinates(cell));
                    LOG.log(Level.SEVERE, msg);
                    throw new MissingMappingException(msg);
                }
                if (builder.length() > 0) {
                    builder.append(", ");
                }
                builder.append(identityMap.get(mapping).toString());
            }
            cell.setCellValue(builder.toString());
        }
    }

    /**
     * Load the Entity Models from the given properties file.
     *
     * @param modelFile the location of the properties file
     * @return A list of Entity Models.
     * @throws IOException when an I/O error occurs while reading the file.
     * @throws DataFormatException when the model file is malformed.
     */
    private List<EntityModel> getEntityModels(final Path modelFile)
            throws IOException, DataFormatException {

        final Properties properties = new Properties();
        properties.load(Files.newBufferedReader(modelFile));

        final List<EntityModel> models = new ArrayList<>();

        final String entities = properties.getProperty("entities", "");

        for (final Entity entity : EntityFactory.getIterable(entities)) {

            final String entityName = entity.getName();
            final String idColumn;
            String propertyName = entityName.concat(".id_column");
            idColumn = properties.getProperty(propertyName, "");
            final String referenceColumns;
            propertyName = entityName.concat(".reference_columns");
            referenceColumns = properties.getProperty(propertyName, "");
            final Map<Field, Entity> referenceMap = new HashMap<>();

            for (final String referenceColumn : referenceColumns.split(",")) {
                if (StringUtils.isNotBlank(referenceColumn)) {
                    storeReference(referenceColumn, referenceMap);
                }
            }
            if (idColumn.isEmpty() && referenceColumns.isEmpty()) {
                LOG.log(Level.WARNING, "Model for entity {0} is empty.",
                        entityName);
            }
            final EntityModel model;
            model = new EntityModel(entity, new Field(idColumn), referenceMap);
            models.add(model);
        }
        return models;
    }

    /**
     * Store the reference mapping in the referenceMap.
     *
     * @param mapping the mapping
     * @param referenceMap the reference map to update.
     * @throws DataFormatException when the data is malformed.
     */
    private void storeReference(final String mapping,
            final Map<Field, Entity> referenceMap) throws DataFormatException {

        final String[] tuple = mapping.trim().split(":");
        if (tuple.length == 2) {
            final String field = tuple[0].trim();
            final String entity = tuple[1].trim();
            if (field.isEmpty()) {
                throw new DataFormatException(
                        "The field cannot be empty in a reference.");
            }
            if (entity.isEmpty()) {
                throw new DataFormatException(
                        "The entity cannot be empty in a reference.");
            }
            referenceMap.put(new Field(tuple[0]), new Entity(tuple[1]));
        } else {
            throw new DataFormatException(
                    "The reference should be defined as colon separated tuple");
        }
    }

    /**
     * Get the column number of the column that holds the given identity field.
     * This method determines the column by matching the column headers on the
     * first row with the name of the field. Comparison is done without
     * consideration of the case.
     *
     * @param sheet the sheet that is to be checked
     * @param identity the identity field to look for.
     * @return -1 if the field is not found otherwise a 0-based column number.
     */
    private int getIdentityColumn(final Sheet sheet, final Field identity) {

        final Row row = sheet.getRow(sheet.getFirstRowNum());

        for (final Cell cell : ExcelUtil.getCells(row)) {
            if (isFieldHeader(identity, cell)) {
                return cell.getColumnIndex();
            }
        }
        return -1;
    }
}
