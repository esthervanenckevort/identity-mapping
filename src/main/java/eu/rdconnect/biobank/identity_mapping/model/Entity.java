/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.model;

/**
 * Container class for entities.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class Entity  extends AbstractStringValue {

    /**
     * Constructor.
     *
     * @param entity the name of the entity.
     */
    public Entity(final String entity) {
        super(entity);
    }

    /**
     * Getter for the name of the entity.
     *
     * @return the name of the entity.
     */
    public String getName() {
        return getValue();
    }
}
