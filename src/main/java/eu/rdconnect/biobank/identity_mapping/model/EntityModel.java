/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.model;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 * The entity model holds the definition of an entity. This class is a
 * lightweight implementation that only holds the bare minimum. That is the
 * entity name, it's identifier field and a map of the entities it refers to
 * with the attribute that holds the reference. No other attributes of the
 * entity are recorded in this class.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class EntityModel {

    /**
     * The name of the entity.
     */
    private final Entity entity;
    /**
     * The name of the field that holds the entity identifier.
     */
    private final Field field;
    /**
     * The map of fields that hold a reference to other entities and the
     * entities to which they refer.
     */
    private final Map<Field, Entity> entityReferences;

    /**
     * Constructor.
     *
     * @param entityName the name of the entity.
     * @param identityField the name of the
     * @param referenceColumns the map of the fields that refer to other
     * entities and the entities to which they refer.
     */
    public EntityModel(final Entity entityName, final Field identityField,
            final Map<Field, Entity> referenceColumns) {
        if (entityName == null || identityField == null
                || referenceColumns == null) {
            throw new IllegalArgumentException(
                    "Constructor arguments cannot be null");
        }
        entity = entityName;
        field = identityField;
        entityReferences = referenceColumns;
    }

    /**
     * Getter for the entity of this model.
     *
     * @return the entity.
     */
    public Entity getEntity() {
        return entity;
    }

    /**
     * Getter for the field that holds the identifier for this entity.
     *
     * @return the field.
     */
    public Field getField() {
        return field;
    }

    /**
     * Getter for the map of the of fields that hold a reference to other
     * entities and the entities to which they refer.
     *
     * @return an unmoddifiable instance of the map.
     */
    public Map<Field, Entity> getEntityReferences() {
        return Collections.unmodifiableMap(entityReferences);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entity, field, entityReferences);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntityModel other = (EntityModel) obj;
        if (!Objects.equals(this.entity, other.entity)) {
            return false;
        }
        if (!Objects.equals(this.field, other.field)) {
            return false;
        }
        return Objects.equals(this.entityReferences, other.entityReferences);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("EntityModel{entity=").append(entity);
        builder.append(", field=").append(field);
        builder.append(", entityReferences=").append(entityReferences);
        builder.append('}');
        return builder.toString();
    }

}
