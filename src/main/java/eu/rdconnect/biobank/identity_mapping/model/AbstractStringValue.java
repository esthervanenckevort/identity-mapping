/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.model;

import java.util.Objects;

/**
 * Abstract base class for String-value-classes. The implementing classes
 * should not add any additional fields, but might add methods for validating.
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public abstract class AbstractStringValue {
    /**
     * The value of the identifier.
     */
    private final String value;

    /**
     * Constructor.
     *
     * @param inputValue the value of the identifier.
     */
    public AbstractStringValue(final String inputValue) {
        this.value = inputValue;
    }

    /**
     * Get the value of the identifier.
     *
     * @return the value.
     */
    public final String getValue() {
        return value;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractStringValue other = (AbstractStringValue) obj;
        return Objects.equals(this.value, other.getValue());
    }

    @Override
    public final String toString() {
        return value;
    }

}
