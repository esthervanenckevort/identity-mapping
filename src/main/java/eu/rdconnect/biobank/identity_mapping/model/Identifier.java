/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.model;

/**
 * Container class for identifiers.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class Identifier extends AbstractStringValue {
    /**
     * Constructor.
     *
     * @param identifier the value of the identifier.
     */
    public Identifier(final String identifier) {
        super(identifier);
    }
}
