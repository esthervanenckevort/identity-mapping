/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rdconnect.biobank.identity_mapping.model;

import java.util.Objects;

/**
 * Class that holds the mapping of an entity to an identifier.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class EntityMapping {

    /**
     * The entity class to which this identifier belongs.
     */
    private final Entity entity;
    /**
     * The identifier for an instance of the entity.
     */
    private final Identifier identifier;

    /**
     * Constructor.
     *
     * @param entityValue the entity.
     * @param identifierValue the identifier for this instance of the entity.
     */
    public EntityMapping(final Entity entityValue,
            final Identifier identifierValue) {
        if (entityValue == null || identifierValue == null) {
            throw new IllegalArgumentException(
                    "Constructor arguments cannot be null");
        }
        entity = entityValue;
        identifier = identifierValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(entity, identifier);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntityMapping other = (EntityMapping) obj;
        if (!Objects.equals(this.entity, other.entity)) {
            return false;
        }
        return Objects.equals(this.identifier, other.identifier);
    }

    /**
     * Get the entity.
     *
     * @return the entity.
     */
    public Entity getEntity() {
        return entity;
    }

    /**
     * Get the identifier.
     *
     * @return the identifier.
     */
    public Identifier getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("EntityMapping{entity=").append(entity);
        builder.append(", identifier=").append(identifier).append('}');
        return builder.toString();
    }
}
